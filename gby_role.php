<?php
/*
Plugin Name: Manager Staff Roles
Plugin URI: http://www.softwareseni.com
Description: This plugin adds Manager and Staff roles
Version: 1.0
Author: Prisantya
Author URI: http://www.softwareseni.com
License: GPL2
*/

register_activation_hook( __FILE__, 'gby_aktivasi' );
register_deactivation_hook( __FILE__, 'gby_uninstallrole' );

add_shortcode('wp7_users','gby_start');

function gby_start($atts) {
    ob_start();
    gby_tampil_user($atts);
    return ob_get_clean();
}

function gby_aktivasi() {
    ob_start();
    gby_add_role();
}

function gby_add_role () {
    add_role('staff','Staff',array(
        'read'=>true,
        'edit_posts'=>true,
        'delete_posts'=>true,
        'detele_published_posts'=>true,
        'publish_posts'=>true,
        'upload_files'=>true,
        'edit_published_posts'=>true,
    ));

    add_role('manager','Manager',array(
        'read'=>true,
        'edit_posts'=>true,
        'delete_posts'=>true,
        'detele_published_posts'=>true,
        'publish_posts'=>true,
        'upload_files'=>true,
        'edit_published_posts'=>true,
        'edit_users'=>true
    ));
}

function gby_uninstallrole() {
    global $wpdb;
    remove_role('staff');
    remove_role('manager');
}



function gby_tampil_user($atts) {
        //set nilai default param
        $values = shortcode_atts( array(
            'role'   	=> array('staff','manager'),
        ), $atts );
        //simpan param ke variabel
        $role=$values['role'];
        
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $limit_per_page=5;
        $current_page=max( 1,$paged);
        $offset = ($current_page - 1) * $limit_per_page;
        $no=$offset+1;
        
        $queries = new WP_User_Query(
            array ('role__in' => $role,
            'number'=>$limit_per_page, 
            'paged'=>$paged,
            'offset'=>$offset
        ));
?>
        <h5> Daftar Manager & Staff </h5>
        <table class="table table-bordered ">
        <thead class="thead-light">
        <tr>
            <th scope="col"> # </th>
            <th scope="col"> Nama </th>
            <th scope="col"> Email </th>
            <th scope="col"> Role </th>
        </tr>
        </thead>
        <tbody>

<?php
    foreach($queries->get_results() as $query) {
        echo "<tr>
        <th scope='row'>".$no." </th>
        <td>".$query->display_name."</td>
        <td>".$query->user_email,"</td>
        <td>".ucfirst($query->roles[0])."</td>
        </tr>";
        $no++;
    }
    echo "</tbody>";
    echo "</table>";

    $big=9999;
    echo paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => $current_page,
        'total' => ceil($queries->get_total()/$limit_per_page)
    ) );
}